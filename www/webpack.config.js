const CopyWebpackPlugin = require("copy-webpack-plugin");
// const CleanWebpackPlugin = require('clean-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const path = require('path');

module.exports = {
    mode: "development",
    entry: "./bootstrap.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bootstrap.js"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader"
            }
        ]
    },
    resolve: {
        extensions: ['.wasm', '.ts', '.js'],
        modules: [
            path.join(__dirname, "."),
            'node_modules'
        ],
        plugins: [
            new TsconfigPathsPlugin({ configFile: path.join(__dirname, 'tsconfig.json') })
        ]
    },
    plugins: [
        new CopyWebpackPlugin(['index.html'])
        // new CleanWebpackPlugin(['dist']),
    ],
};
