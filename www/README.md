# `game-of-life-web`

> Javascript interface for Game of Life

## Usage

```
yarn install
yarn start
```

## License

Licensed under Beerware license.

