<meta charset="utf-8"/>

# `Game of life as rust-wasm proof-of-concept`

Project made with tutorial https://rustwasm.github.io/book/game-of-life/introduction.html with few modifications.

## Usage

### Build rust code with wasm-pack

```
wasm-pack build
```

### Build JS code with webpack

```
cd www
yarn install
yarn start
```

### Test in Headless Browsers with `wasm-pack test`

```
wasm-pack test --headless --firefox
```

